#!/bin/bash
# build file
GOPATH=$(go env GOPATH)
version=$(git describe --always --long --dirty --long)
if [ version == "HEAD" ] ; then
	version=$( git log -1 --pretty=format:"%h")
fi
go build -ldflags="-X main.version=${version}" -o $GOPATH/bin/radosgw_exporter
