package main

import (
	"flag"
	"net/http"
	"sync"
//	"encoding/json"
//	"os"
//	"io"
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/QuentinPerez/go-radosgw/pkg/api"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	version = "undefined"
	metricsPrefix = "radosgw"
	RGWScrapeSuccessDesc = prometheus.NewDesc(
		prometheus.BuildFQName(metricsPrefix, "", "scrape_success"),
		"Whether scraping the RadosGW stats was successful.",
		nil, nil)
	wg sync.WaitGroup
	gUsage, gBuckets, gQuotas bool
)



func main() {
	
	var (
		listenAddress = flag.String("web.listen-address", ":9242", "Address to listen on for web interface and telemetry.")
		metricsPath   = flag.String("web.telemetry-path", "/metrics", "Path under which to expose metrics.")
		endpoint      = flag.String("s3.endpoint", "http://127.0.0.1:7480/", "RGW URL")
		adminPrefix   = flag.String("s3.adminprefix", "admin", "Admin entry point")
		accessKey     = flag.String("s3.access", "", "S3 access key")
		secretKey     = flag.String("s3.secret", "", "S3 secrest key")
		logLevel      = flag.String("loglevel", "info", "Log level: fatal, error, warning, info, debug.")
		gatherUsage   = flag.Bool("gather.usage", true, "Gather usage, if false - quotas alredy false")
		gatherBuckets = flag.Bool("gather.buckets", true, "Gather buckets statistics")
		gatherQuotas  = flag.Bool("gather.quotas", true, "Gather quotas information")
		//gatherUsers   = flag.String("loglevel", "info", "Log level: fatal, error, warning, info, debug.")
	)
	flag.Parse()
	
	level, err := log.ParseLevel(*logLevel)
	if err != nil{
		level = log.InfoLevel
		log.Error("Couldn't  parse logLevel, set default - Info")
	}
	
	if *endpoint == "" || *accessKey == "" || *secretKey == "" {
		log.Fatal("Endpoint and keys is required")
	}
	
	gUsage = *gatherUsage
	gBuckets = *gatherBuckets
	gQuotas = *gatherQuotas
	
	log.SetLevel(level)
	log.Info("Starting radosgw_exporter (" + version + ")")

	exporter, err := newRGWExporter(*endpoint, *accessKey, *secretKey, *adminPrefix)
	if err != nil {
		log.Fatal(err)
	}
	prometheus.MustRegister(exporter)

	http.Handle(*metricsPath, prometheus.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`
			<html>
			<head><title>RadosGW Exporter</title></head>
			<body>
			<h1>RadosGW Exporter</h1>
			<p><a href='` + *metricsPath + `'>Metrics</a></p>
			</body>
			</html>`))
	})
	log.Info("Listening on address:port => ", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

//Define a struct for you collector that contains pointers
//to prometheus descriptors for each metric you wish to expose.
//Note you can also include fields of other types if they provide utility
//but we just won't be exposing them as metrics.
type RGWExporter struct {
	collectLock sync.Mutex
	endpoint string
	accessKey string
	secretKey string
	adminPrefix string
}

func newRGWExporter(endpoint, accessKey, secretKey, adminPrefix string) (*RGWExporter, error) {
	return &RGWExporter{
		endpoint: endpoint,
		accessKey: accessKey,
		secretKey: secretKey,
		adminPrefix: adminPrefix,
	}, nil
}


//Each and every collector must implement the Describe function.
//It essentially writes all descriptors to the prometheus desc channel.
func (e *RGWExporter) Describe(ch chan<- *prometheus.Desc) {
	//Update this section with the each metric you create for a given collector

	ch <- RGWScrapeSuccessDesc
}

//Collect implements required collect function for all promehteus collectors
func (e *RGWExporter) Collect(ch chan<- prometheus.Metric) {
	//Implement logic here to determine proper metric value to return to prometheus
	//for each descriptor or call other functions that do so.
	e.collectLock.Lock()
	err := collectFromRadosgw(e.endpoint, e.accessKey, e.secretKey, e.adminPrefix, ch)
	e.collectLock.Unlock()
	
	if err == nil {
		ch <- prometheus.MustNewConstMetric(RGWScrapeSuccessDesc, prometheus.GaugeValue, 1.0)
	} else {
		log.Error("Failed to gather stats: ", err)
		ch <- prometheus.MustNewConstMetric(RGWScrapeSuccessDesc, prometheus.GaugeValue, 0.0)
	}
}

// Scrape information from Admin API of Ceph RadosGW
func collectFromRadosgw(endpoint, accessKey, secretKey, adminPrefix string, ch chan <- prometheus.Metric) error {
	api, err := radosAPI.New(endpoint, accessKey, secretKey, adminPrefix)
	if err != nil {
		log.Fatal(err)
		return err
	}
	
	if gUsage {
		wg.Add(1)
		go collectUsage(api, ch)
	}
	if gBuckets {
		wg.Add(1)
		go collectBuckets(api, ch)
	}
	
	wg.Wait()
	return nil
}


func collectUsage(api *radosAPI.API, ch chan <- prometheus.Metric) error {
	defer wg.Done()
	usage, err := api.GetUsage(radosAPI.UsageConfig{ShowSummary: true})
	if err != nil {
		log.Error("Failed to gather usage: ", err)
		return err
	}
	
	for _, item := range usage.Summary {
		variableLabels := []string{"UID"}
		// fill total stats
		fillUsageStat(
			"total",
			item.Total.BytesReceived,
			item.Total.BytesSent,
			item.Total.Ops,
			item.Total.SuccessfulOps,
			ch,
			nil,
			variableLabels,
			item.User,	
		)
		
		//  fill by categories
		for _, cat := range item.Categories {
			fillUsageStat(
				cat.Category,
				cat.BytesReceived,
				cat.BytesSent,
				cat.Ops,
				cat.SuccessfulOps,
				ch,
				nil,
				variableLabels,
				item.User,	
			)
		}
		
		// collect users meta
		if item.User != "anonymous" {
			err = collectUserInfo(api, item.User, ch)
			
			if gQuotas {
				wg.Add(1)
				go collectQuotas(api, item.User, ch)
			}
		}
	}
	
	return nil
}


// simple operations by creatin metrics
func fillUsageStat(category string, bytesRecieved, bytesSent, ops, succOps int ,ch chan <- prometheus.Metric, constLabels prometheus.Labels, variableLabels []string, labelValues...string){
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", category + "_recieved_bytes"),
				"",
				variableLabels,
				constLabels,
				),
			prometheus.GaugeValue,
			float64(bytesRecieved),
			labelValues...,
		)
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", category + "_sent_bytes"),
				"",
				variableLabels,
				constLabels,
				),
			prometheus.GaugeValue,
			float64(bytesSent),
			labelValues...,
		)
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", category + "_ops"),
				"",
				variableLabels,
				constLabels,
				),
			prometheus.GaugeValue,
			float64(ops),
			labelValues...,
		)
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", category + "_successful_ops"),
				"",
				variableLabels,
				constLabels,
				),
			prometheus.GaugeValue,
			float64(succOps),
			labelValues...,
		)
}

func collectBuckets(api *radosAPI.API, ch chan <- prometheus.Metric) error {
	defer wg.Done()
	buckets, err := api.GetBucket(radosAPI.BucketConfig{Stats: true})
	
	if err != nil {
		log.Error("Failed to gather buckets stats: ", err)
		return err
	}
	
	for _, bucket := range buckets {
		// metadata
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "buckets", "metadata"),
				"",
				[]string{"bucket", "owner", "datapool", "indexpool"},
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			1.0,
			bucket.Stats.Bucket, bucket.Stats.Owner, bucket.Stats.Pool, bucket.Stats.IndexPool,
		)
		
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", "bucket_objects"),
				"",
				[]string{"bucket"},
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(bucket.Stats.Usage.RgwMain.NumObjects),
			bucket.Stats.Bucket,
		)
		
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", "bucket_size_kilobytes"),
				"",
				[]string{"bucket"},
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(bucket.Stats.Usage.RgwMain.SizeKb),
			bucket.Stats.Bucket,
		)
		
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "usage", "bucket_size_actual_kilobytes"),
				"",
				[]string{"bucket"},
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(bucket.Stats.Usage.RgwMain.SizeKbActual),
			bucket.Stats.Bucket,
		)
		
		if bucket.Stats.BucketQuota.Enabled {
			ch <- prometheus.MustNewConstMetric(
				prometheus.NewDesc(
					prometheus.BuildFQName(metricsPrefix, "bucket_quotas", "total_objects"),
					"",
					[]string{"bucket"},
					prometheus.Labels{},
					),
				prometheus.GaugeValue,
				float64(bucket.Stats.BucketQuota.MaxObjects),
				bucket.Stats.Bucket,
			)
			
			ch <- prometheus.MustNewConstMetric(
				prometheus.NewDesc(
					prometheus.BuildFQName(metricsPrefix, "bucket_quotas", "total_size_kilobytes"),
					"",
					[]string{"bucket"},
					prometheus.Labels{},
					),
				prometheus.GaugeValue,
				float64(bucket.Stats.BucketQuota.MaxSizeKb),
				bucket.Stats.Bucket,
			)
		}
	}
	
	return nil
}


// Collect quotas from specified user
// MB should return all disable quotas too
func collectQuotas(api *radosAPI.API, UID string, ch chan <- prometheus.Metric) error {
	defer wg.Done()
	if UID == "" {
		err := fmt.Errorf("UID is required", nil)
		log.Error(err)
		return err
	}
	quota, err := api.GetQuotas(radosAPI.QuotaConfig{UID: UID})
	if err != nil {
		log.Error("Failed to gather user", UID, "qoutas: ", err)
		return err
	}
	variableLabels := []string{"UID"}
	if quota.BucketQuota.Enabled {
		// bucket_quota max objects
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user_quotas", "bucket_objects"),
				"",
				variableLabels,
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(quota.BucketQuota.MaxObjects),
			UID,
		)
		// bucket_quota max size
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user_quotas", "bucket_size_kilobytes"),
				"",
				variableLabels,
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(quota.BucketQuota.MaxSizeKb),
			UID,
		)
	}
	
	if quota.UserQuota.Enabled {
		// user_quota max objects
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user_quotas", "total_objects"),
				"",
				variableLabels,
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(quota.UserQuota.MaxObjects),
			UID,
		)
		// user_quota max size
		ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user_quotas", "total_size_kilobytes"),
				"",
				variableLabels,
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(quota.UserQuota.MaxSizeKb),
			UID,
		)
	}
	
	return nil
}


// Collect user metadata such as email, display name and etc.
func collectUserInfo(api *radosAPI.API, UID string, ch chan <- prometheus.Metric) error {
	user, err := api.GetUser(UID)
	if err != nil {
		return err
	}
	variableLabels := []string{"UID", "displayName", "email"}
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user", "metadata"),
				"",
				variableLabels,
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			1.0,
			UID, user.DisplayName, user.Email,
		)
	ch <- prometheus.MustNewConstMetric(
			prometheus.NewDesc(
				prometheus.BuildFQName(metricsPrefix, "user", "suspended"),
				"",
				[]string{"UID"},
				prometheus.Labels{},
				),
			prometheus.GaugeValue,
			float64(user.Suspended),
			UID,
		)
	
	return nil
}
